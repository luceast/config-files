# config-files ✏️
Here I store my config files. Feel free to use them.
I also created a neat [installer](setup.sh) to easiely copy the configfiles to their belonged loaction or delete them. \

- [installation](#installation)


## Installation
 • [help](#help)\
 • [install](#install)\
 • [delete](#delete) 

```
git clone https://github.com/LucEast/config-files.git; cd config-files
./setup.sh
```
**You will need to add arguments (-i|-d) to the script to work.**\
You can navigate through the script via the up and down arrow keys.\
To select a config-file, press space.\
Once selected, press Return to commit.

### help
![help](https://raw.githubusercontent.com/LucEast/config-files/main/screenshots/help.png)

### install
```
./setup.sh -i
```
![install](https://raw.githubusercontent.com/LucEast/config-files/main/screenshots/install.png)

### delete
```
./setup.sh -d
```
![delete](https://raw.githubusercontent.com/LucEast/config-files/main/screenshots/delete.png)





![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=LucEast&repo=dotfiles&title_color=3e83c8&text_color=00cb71&icon_color=299bab&bg_color=171717&hide_border=true)
